﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;

using Microsoft.AspNetCore;
namespace RESTClient.Json
{
    // compile with: -doc:JsonClient.xml
    /// <summary>
    ///     <para>JsonClient provides Async REST functionality (GET, POST, PUT, DELETE) for communicating with a JSON API Services.</para>
    ///     <para>JsonClient uses the <see cref="HttpClientFactory"/> to lower system resource usage and avoid client side socket exhaustion during high volume usage.</para>
    ///     <para>JsonClient supports ASP.NET Core 2.1 Dependency Injection design pattern. Exmaple usage below.</para>
    ///     <para>References:
    ///         https://docs.microsoft.com/en-us/dotnet/standard/microservices-architecture/implement-resilient-applications/use-httpclientfactory-to-implement-resilient-http-requests
    ///         https://aspnetmonsters.com/2016/08/2016-08-27-httpclientwrong/
    ///     </para>
    ///     <example>
    ///     <para>Startup.cs - ConfigureServices</para>
    ///         <code>
    ///             var s = services.AddHttpClient("example", client=>{
    ///                 client.BaseAddress = new Uri("https://630studios.com/jsonclient/");
    ///                 client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
    ///                 client.DefaultRequestHeaders.UserAgent.ParseAdd("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36");
    ///             });
    ///     
    ///             services.AddScoped<RESTClients.Json.JsonClient>(p=> new RESTClients.Json.JsonClient(p.GetService<IHttpClientFactory>(), "example"));
    ///         </code>
    ///</example>



    public class JsonClient : IRESTClient
    {

        protected IHttpClientFactory _clientFactory;
        protected string _clientName = "";

        public JsonClient(IHttpClientFactory factory, string clientName)
        {
            _clientFactory = factory;
            _clientName = clientName;
        }

        #region GET Async

        /// <summary> Async GET method using IAPIMessage and IAPIREsponse objects. Takes an endpoint string and a query string returning and instance of TResponse. </summary>
        /// <typeparam name="TResponse">Expected response type. Must inherit <see cref="IAPIResponse"/> interface.</typeparam>
        /// <param name="endpoint">Target endpoint to send the GET request to. Should not include the baseURI.</param>
        /// <param name="queryString">Additional Query string to append to the endpoint. Must be formatted properly.</param>
        /// <returns>An instance of TResponse.</returns>

        public async Task<TResponse> GET_Async<TResponse>(string endpoint, string queryString) where TResponse : IAPIResponse, new()
        {
            HttpClient client = _clientFactory.CreateClient(_clientName);
            HttpResponseMessage response = await client.GetAsync(endpoint + queryString);
            return await ValidateResponseAsync<TResponse>(response);
        }


        /// <summary> Async GET method using generic data types. Takes an endpoint string and a query string returning and instance of TResponse. </summary>
        /// <typeparam name="TResponse">Expected response type. Must inherit <see cref="IAPIResponse"/> interface.</typeparam>
        /// <param name="endpoint">Target endpoint to send the GET request to. Should not include the baseURI.</param>
        /// <param name="queryString">Additional Query string to append to the endpoint. Must be formatted properly.</param>
        /// <returns>An instance of TResponse.</returns>
        public async Task<TResponse> GETGeneric_Async<TResponse>(string endpoint, string queryString)
        {
            HttpClient client = _clientFactory.CreateClient(_clientName);
            HttpResponseMessage response = await client.GetAsync(endpoint + queryString);
            return await ValidateGenericResponseAsync<TResponse>(response);
        }

        /// <summary> Returns the raw string data from a GET request. </summary>
        /// <param name="endpoint">Target endpoint to send the GET request to. Should not include the baseURI</param>
        /// <param name="queryString">Additional Query string to append to the endpoint. Must be formatted properly.</param>
        /// <returns>Returns the contents of the request as a string. If the request was not sucessful null is returned.</returns>

        public async Task<string> GETRaw_Async(string endpoint, string queryString)
        {
            string output = null;
            HttpClient client = _clientFactory.CreateClient(_clientName);
            HttpResponseMessage response = await client.GetAsync(endpoint + queryString);

            if (response.IsSuccessStatusCode)
                output = await response.Content.ReadAsStringAsync();

            return output;
        }

        #endregion

        #region POST Async

        /// <summary> Async POST method using IAPIREsponse objects. Takes an endpoint and a data object and POSTs the data to the endpoint. </summary>
        /// <typeparam name="TResponse">Expected response type. Must inherit <see cref="IAPIResponse"/> interface.</typeparam>
        /// <typeparam name="TData">The type of data being supplied.</typeparam>
        /// <param name="endpoint">Target endpoint to send the POST request to. Should not include the baseURI</param>
        /// <param name="data">The data to POST to the endpoint.</param>
        /// <returns>Instance of TResponse</returns>

        public async Task<TResponse> POST_Async<TResponse, TData>(string endpoint, TData data) where TResponse : IAPIResponse, new()
        {
            var client = _clientFactory.CreateClient(_clientName);
            
            StringContent sc = new StringContent(JsonConvert.SerializeObject(data), System.Text.Encoding.UTF8, "application/json");
            HttpResponseMessage response = await client.PostAsync(endpoint, sc);
            
            return await ValidateResponseAsync<TResponse>(response);
        }

        /// <summary> Async POST method using generic response data types. Takes an endpoint and a data object and POSTs the data to the endpoint. </summary>
        /// <typeparam name="TResponse">Expected response type. Must inherit <see cref="IAPIResponse"/> interface.</typeparam>
        /// <typeparam name="TData">The type of data being supplied.</typeparam>
        /// <param name="endpoint">Target endpoint to send the POST request to. Should not include the baseURI</param>
        /// <param name="data">The data to POST to the endpoint.</param>
        /// <returns>Instance of TResponse</returns>

        public async Task<TResponse> POSTGeneric_Async<TResponse, TData>(string endpoint, TData data)
        {
            HttpClient client = _clientFactory.CreateClient(_clientName);
            StringContent sc = new StringContent(JsonConvert.SerializeObject(data), System.Text.Encoding.UTF8, "application/json");
            HttpResponseMessage response = await client.PostAsync(endpoint, sc);
            return await ValidateGenericResponseAsync<TResponse>(response);
        }

        /// <summary> Async POST method returning the response as a string. Takes an endpoint and a data object and POSTs the data to the endpoint. </summary>
        /// <typeparam name="TResponse">Expected response type. Must inherit <see cref="IAPIResponse"/> interface.</typeparam>
        /// <typeparam name="TData">The type of data being supplied.</typeparam>
        /// <param name="endpoint">Target endpoint to send the POST request to. Should not include the baseURI</param>
        /// <param name="data">The data to POST to the endpoint.</param>
        /// <returns>Instance of TResponse</returns>
        public async Task<string> POSTRaw_Async<TData>(string endpoint, TData data)
        {
            HttpClient client = _clientFactory.CreateClient(_clientName);
            StringContent sc = new StringContent(JsonConvert.SerializeObject(data), System.Text.Encoding.UTF8, "application/json");
            HttpResponseMessage response = await client.PostAsync(endpoint, sc);

            if (response.IsSuccessStatusCode)
                return await response.Content.ReadAsStringAsync();

            return null;
        }

        #endregion

        #region PUT Async

        /// <summary> Aysnc PUT method using IAPIMessage and IAPIREsponse objects. Takes and endpoint and a data object and PUTs the data to the endpoint. </summary>
        /// <typeparam name="TResponse">Expected response type. Must inherit <see cref="IAPIResponse"/> interface.</typeparam>
        /// <typeparam name="TData">The type of data being supplied.</typeparam>
        /// <param name="endpoint">Target endpoint to send the PUT request to. Should not include the baseURI</param>
        /// <param name="data">The data to PUT to the endpoint.</param>
        /// <returns>Instance of TResponse</returns>

        public async Task<TResponse> PUT_Async<TResponse, TData>(string endpoint, TData data) where TResponse : IAPIResponse, new()
        {
            HttpClient client = _clientFactory.CreateClient(_clientName);
            HttpResponseMessage response = await client.PutAsync(endpoint, new StringContent(JsonConvert.SerializeObject(data), System.Text.Encoding.UTF8, "application/json"));
            return await ValidateResponseAsync<TResponse>(response);
        }

        /// <summary> Aysnc PUT method using generic data types. Takes and endpoint and a data object and PUTs the data to the endpoint. </summary>
        /// <typeparam name="TResponse">Expected response type. Must inherit <see cref="IAPIResponse"/> interface.</typeparam>
        /// <typeparam name="TData">The type of data being supplied.</typeparam>
        /// <param name="endpoint">Target endpoint to send the PUT request to. Should not include the baseURI</param>
        /// <param name="data">The data to PUT to the endpoint.</param>
        /// <returns>Instance of TResponse</returns>

        public async Task<TResponse> PUTGeneric_Async<TResponse, TData>(string endpoint, TData data)
        {
            HttpClient client = _clientFactory.CreateClient(_clientName);
            HttpResponseMessage response = await client.PutAsync(endpoint, new StringContent(JsonConvert.SerializeObject(data), System.Text.Encoding.UTF8, "application/json"));
            return await ValidateGenericResponseAsync<TResponse>(response);
        }

        #endregion

        #region DELETE Async

        /// <summary> Aync Delete Method using IAPIMessage and IAPIREsponse objects. Sends a DELETE command to the endpoint + queryString. </summary>
        /// <typeparam name="TResponse">Expected response type. Must inherit <see cref="IAPIResponse"/> interface.</typeparam>
        /// <param name="endpoint">Target endpoint to send the DELETE request to. Should not include the baseURI</param>
        /// <param name="queryString">Additional Query string to append to the endpoint. Must be formatted properly.</param>
        /// <returns>Instance of TResponse</returns>

        public async Task<TResponse> DELETE_Async<TResponse>(string endpoint, string query) where TResponse : IAPIResponse, new()
        {
            HttpClient client = _clientFactory.CreateClient(_clientName);
            HttpResponseMessage response = await client.DeleteAsync(endpoint + query);
            return await ValidateResponseAsync<TResponse>(response);
        }

        /// <summary>Aync Delete Method for generic data types. Sends a DELETE command to the endpoint + queryString. </summary>
        /// <typeparam name="TResponse">Expected response type. Must inherit <see cref="IAPIResponse"/> interface.</typeparam>
        /// <param name="endpoint">Target endpoint to send the DELETE request to. Should not include the baseURI</param>
        /// <param name="queryString">Additional Query string to append to the endpoint. Must be formatted properly.</param>
        /// <returns>Instance of TResponse</returns>

        public async Task<TResponse> DELETEGeneric_Async<TResponse>(string endpoint, string query)
        {
            HttpClient client = _clientFactory.CreateClient(_clientName);
            HttpResponseMessage response = await client.DeleteAsync(endpoint + query);
            return await ValidateGenericResponseAsync<TResponse>(response);
        }

        #endregion

        #region ValidateResponse

        protected async Task<T> ValidateResponseAsync<T>(HttpResponseMessage response) where T : IAPIResponse, new()
        {

            T output;

            if (response.IsSuccessStatusCode)
            {
                /* 
                 * Everything looks good. Attempt to decode json data into TResponse, and set the Action as successful.
                 */
                output = JsonConvert.DeserializeObject<T>(await response.Content.ReadAsStringAsync());
                output.ActionSuccessful = true;
            }
            else
            {
                /* 
                 * If we do not get a success status code. We return a customized
                 * TResonse instance with information about what went wrong
                 */
                output = new T { ActionSuccessful = false, StatusCode = response.StatusCode, ReasonPhrase = response.ReasonPhrase };
            }
            return output;
        }

        protected async Task<T> ValidateGenericResponseAsync<T>(HttpResponseMessage response)
        {
            /*
             * Initialize output to default value to be used in the event the
             * response does not return a successful status code.
             */
            T output = default(T);

            if (response.IsSuccessStatusCode)
            {
                /* 
                 * Everything looks good. Attempt to decode json data into output.
                 */
                output = JsonConvert.DeserializeObject<T>(await response.Content.ReadAsStringAsync());
            }
            return output;
        }

        #endregion

    }
}
