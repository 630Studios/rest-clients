# JsonClient
 
* JsonClient provides Async REST functionality (GET, POST, PUT, DELETE) for communicating with a JSON API Services and is intended as a lightweight framework to build REST API consumers.
 * JsonClient uses the [HttpClientFactory](https://docs.microsoft.com/en-us/previous-versions/aspnet/hh995280(v%3Dvs.118)) to lower system resource usage and avoid client side socket exhaustion during high volume usage.
 * JsonClient supports ASP.NET Core 2.1 Dependency Injection design pattern. Example usage below.


         
         
#### Startup.cs - ConfigureServices()
```cs
var s = services.AddHttpClient("example", client=>{
    client.BaseAddress = new Uri("https://example.com/");
    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
    client.DefaultRequestHeaders.UserAgent.ParseAdd("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36");
});

services.AddScoped<RESTClients.Json.JsonClient>(p=> new RESTClients.Json.JsonClient(p.GetService<IHttpClientFactory>(), "example"));
```
         
                 
#### References:
[Use httpclientfactory to implement resilient http requests](https://docs.microsoft.com/en-us/dotnet/standard/microservices-architecture/implement-resilient-applications/use-httpclientfactory-to-implement-resilient-http-requests)
[Aspnetmonsters.com Article](https://aspnetmonsters.com/2016/08/2016-08-27-httpclientwrong/)
   

