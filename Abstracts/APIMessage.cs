﻿using System;
using System.Web;
using System.Collections.Specialized;
using System.Linq;
using Newtonsoft.Json;
using System.Reflection;


namespace RESTClient
{
    [Serializable]
    [JsonObject]
    public abstract class APIMessage: IAPIMessage
    {
        public virtual string ToQueryString()
        {
            throw new NotImplementedException("ToQueryString() Not Implimented: Class inheriting APIMessage should impliment this.");
        }

        /// <summary>
        /// Very lazy method to build up a query string.
        /// Gathers non null valued properties marked with <see cref="JsonPropertyAttribute"/>
        /// whose value passes string.IsNullOrEmpty() check and builds the query string from the results.
        /// </summary>
        /// <returns></returns>
        public string GetReflectedQueryString()
        {
            
            PropertyInfo[] properties = GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance | BindingFlags.FlattenHierarchy)
                .Where(p => Attribute.IsDefined(p, typeof(JsonPropertyAttribute)) && p.GetValue(this) != null).ToArray();

            if (properties.Length > 0)
            {
                NameValueCollection data = new NameValueCollection();

                foreach (PropertyInfo prop in properties)
                {
                    string value = prop.GetValue(this).ToString();
                    if (!string.IsNullOrEmpty(value))
                        data.Add(prop.Name, prop.GetValue(this).ToString());
                }
                return BuildQueryString(data);
            }
            else
            {
                return "";
            }
                        

        }


        protected string BuildQueryString(NameValueCollection nvc)
        {

            var array = (from key in nvc.AllKeys
                         from value in nvc.GetValues(key)
                         where !string.IsNullOrEmpty(value)
                         select string.Format("{0}={1}", HttpUtility.UrlEncode(key), HttpUtility.UrlEncode(value)))
                .ToArray();
            return "?" + string.Join("&", array);
        }
    }
}
