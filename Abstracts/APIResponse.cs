﻿using System.Net;
using Newtonsoft.Json;

namespace RESTClient
{



    public abstract class APIResponse: IAPIResponse
    {
        [JsonIgnore]
        public bool ActionSuccessful { get; set; }
        [JsonIgnore]
        public HttpStatusCode StatusCode { get; set; }
        [JsonIgnore]
        public string ReasonPhrase { get; set; }
    }


}
