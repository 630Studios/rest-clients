﻿using System;
using System.Web;
using System.Collections.Specialized;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Reflection;

namespace RESTClient
{
    public interface IAPIMessage
    {
        string ToQueryString();
        string GetReflectedQueryString();
        
    }
}
