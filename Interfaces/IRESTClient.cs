﻿using System.Threading.Tasks;

namespace RESTClient
{
    public interface IRESTClient
    {
        Task<TResponse> DELETEGeneric_Async<TResponse>(string endpoint, string queryString);
        Task<TResponse> DELETE_Async<TResponse>(string endpoint, string query) where TResponse : IAPIResponse, new();
        Task<TResponse> GETGeneric_Async<TResponse>(string endpoint, string queryString);
        Task<string> GETRaw_Async(string endpoint, string queryString);
        Task<TResponse> GET_Async<TResponse>(string endpoint, string queryString) where TResponse : IAPIResponse, new();
        Task<TResponse> POSTGeneric_Async<TResponse, TData>(string endpoint, TData data);
        Task<string> POSTRaw_Async<TData>(string endpoint, TData data);
        Task<TResponse> POST_Async<TResponse, TData>(string endpoint, TData data) where TResponse : IAPIResponse, new();
        Task<TResponse> PUTGeneric_Async<TResponse, TData>(string endpoint, TData data);
        Task<TResponse> PUT_Async<TResponse, TData>(string endpoint, TData data) where TResponse : IAPIResponse, new();
    }
}