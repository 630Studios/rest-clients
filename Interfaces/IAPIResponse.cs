﻿
using System.Net;
using Newtonsoft.Json;

namespace RESTClient
{

    /// <summary>
    /// 
    /// </summary>
    public interface IAPIResponse
    {
        [JsonIgnore]
        bool ActionSuccessful { get; set; }
        [JsonIgnore]
        HttpStatusCode StatusCode { get; set; }
        [JsonIgnore]
        string ReasonPhrase { get; set; }
    }


}
